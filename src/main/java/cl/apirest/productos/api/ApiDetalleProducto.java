package cl.apirest.productos.api;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import cl.apirest.productos.model.entity.DetalleProducto;
import cl.apirest.productos.service.IDetalleService;
import cl.apirest.productos.service.config.SwaggerConfig;
import cl.apirest.productos.service.error.ResponseHandlerJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@CrossOrigin(origins = {"http://localhost:4200"})
@Api(value = "Controlador Detalle de Productos")
@Import(SwaggerConfig.class)
@RestController
@RequestMapping("/api/v1")
public class ApiDetalleProducto {
	
	/**
	 * private static final Logger log = LoggerFactory.getLogger(ApiDetalleProducto.class);
	 */
	
    @Autowired
    private IDetalleService detalleService;
    
    @ApiOperation(value = "Lista completa Detalle de Productos", response = ApiDetalleProducto.class)
    @GetMapping("/getListaProductos")
    public ResponseEntity<Object> getDetalleProductos() {
        try {
            return ResponseHandlerJson.generateResponse(HttpStatus.OK, true, "Success", detalleService.findAll());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No se pudo encontrar Datos!", ex);
        }
    }
    
    @ApiOperation(value = "Lista Detalle de Productos por ID", response = ApiDetalleProducto.class)
    @GetMapping("/getProductosId/{idPro}")
    public ResponseEntity<Object> getProductosById(@PathVariable Integer idPro) {

        Optional<DetalleProducto> busquedaProducto = detalleService.findByProdId(idPro);

        if(busquedaProducto.isPresent()) {
            return ResponseHandlerJson.generateResponse(HttpStatus.OK, true, "Success, seek", busquedaProducto.get());
        } else {
            return ResponseHandlerJson.generateResponse(HttpStatus.NOT_FOUND, false, "Success, Seek", null);
        }
    }

    @ApiOperation(value = "Agregar nuevo Producto", response = ApiDetalleProducto.class)
    @PostMapping("/saveProducto")
    public ResponseEntity<Object> insertaProducto(@RequestBody DetalleProducto newProducto) {

        try {
            return ResponseHandlerJson.generateResponse(HttpStatus.OK, true, "Success, save", detalleService.save(newProducto));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No se pudo encontrar Datos!", ex);
        }
    }

    @ApiOperation(value = "Acualiza Detalle de Producto", response = ApiDetalleProducto.class)
    @PutMapping("/updateProducto/{idPro}")
    public ResponseEntity<Object> actualizaProducto(@RequestBody DetalleProducto updateProducto, @PathVariable Integer idPro) {

        Optional<DetalleProducto> busquedaProducto = detalleService.findByProdId(idPro);

        if(busquedaProducto.isPresent()) {
            return ResponseHandlerJson.generateResponse(HttpStatus.CREATED, true, "Success, save", detalleService.save(updateProducto));
        } else {
            return ResponseHandlerJson.generateResponse(HttpStatus.NOT_FOUND, false, "Success, Seek", null);
        }

    }

    @ApiOperation(value = "Borra Producto por ID", response = ApiDetalleProducto.class)
    @DeleteMapping("/deleteProducto/{idPro}")
    public void borraProducto(@PathVariable Integer idPro) {

        try {
            detalleService.delete(idPro);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No se pudo borrar Datos!", ex);
        }

    }    
    

}
