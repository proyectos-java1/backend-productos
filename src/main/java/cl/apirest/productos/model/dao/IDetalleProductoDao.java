package cl.apirest.productos.model.dao;

import org.springframework.data.repository.CrudRepository;
import cl.apirest.productos.model.entity.DetalleProducto;

public interface IDetalleProductoDao extends CrudRepository <DetalleProducto, Integer>{

    DetalleProducto findByProductoId(Integer proId);
    
}