package cl.apirest.productos.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;

@Setter
@Getter
@Entity
@Table(name = "detalle_productos")
public class DetalleProducto implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    
    private Integer productoId;

    private String productoNombre;
    
    private Integer productoCantidad;

    private String productoDescripcion;
    
    private Integer productoUbicacion;
    
    private Date creationDate;
    
    
}