package cl.apirest.productos.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.apirest.productos.model.dao.IDetalleProductoDao;
import cl.apirest.productos.model.entity.DetalleProducto;


@Service
public class DetalleServiceImpl implements IDetalleService {
	
	@Autowired
    private IDetalleProductoDao productoDao;
	
	@Override
	public List<DetalleProducto> findAll() {	
		return (List<DetalleProducto>) productoDao.findAll();
	}

	@Override
	public Optional<DetalleProducto> findById(Integer id) {		
		return productoDao.findById(id);
	}
	
	@Override
	public Optional<DetalleProducto>  findByProdId(Integer proId) {		
		return Optional.ofNullable(productoDao.findByProductoId(proId));
	}
	
    @Override
    public DetalleProducto save(DetalleProducto detalleProducto) {
        return productoDao.save(detalleProducto);
    }

    @Override
    public void delete(Integer id) {
        productoDao.deleteById(id);
    }	
	
}
