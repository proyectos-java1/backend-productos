package cl.apirest.productos.service;

import java.util.List;
import java.util.Optional;

import cl.apirest.productos.model.entity.DetalleProducto;


public interface IDetalleService {
	
    List<DetalleProducto> findAll();    
    
    Optional<DetalleProducto> findById(Integer id);
    
    Optional<DetalleProducto> findByProdId(Integer proId);    

    DetalleProducto save(DetalleProducto detalleProducto);

    void delete(Integer id);

}
